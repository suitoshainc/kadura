<?php defined("C5_EXECUTE") or die("Access Denied."); ?>
<?php if (isset($ttl) && trim($ttl) != "") { ?>
    <?php echo h($ttl); ?><?php } ?>
<?php if (!empty($images_items)) { ?>
    <?php foreach ($images_items as $images_item_key => $images_item) { ?><?php if ($images_item["img"]) { ?>
    <img src="<?php echo $images_item["img"]->getURL(); ?>" alt="<?php echo $images_item["img"]->getTitle(); ?>"/><?php } ?><?php } ?><?php } ?>
<?php if (isset($content) && trim($content) != "") { ?>
    <?php echo $content; ?><?php } ?>