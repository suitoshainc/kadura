<?php defined("C5_EXECUTE") or die("Access Denied."); ?>
<div class="layout--2col--ttl_and_text">
	<div class="col">
	<?php if (isset($ttl) && trim($ttl) != "") { ?>
		<h3 class="ttl ttl--underline"><?php echo h($ttl); ?></h3>
	<?php } ?>
	</div>
	<div class="col">
		<?php if (isset($content) && trim($content) != "") { ?>
		<?php echo $content; ?><?php } ?>
	</div>
</div>