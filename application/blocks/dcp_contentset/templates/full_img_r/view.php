<?php defined("C5_EXECUTE") or die("Access Denied."); ?>

<?php if (!empty($images_items)) { ?>
	<div class="layout--2col--imgRight">
		<?php foreach ($images_items as $images_item_key => $images_item) { ?>
			<?php if ($images_item["img"]) { ?>
				<div class="col img" style="background-image:url(<?php echo $images_item["img"]->getURL(); ?>);">
					<div class="inner">
					<img src="<?php echo $images_item["img"]->getURL(); ?>" alt="<?php echo $images_item["img"]->getTitle(); ?>"/>
					</div>
				</div>
				
			<?php } ?>
		<?php } ?>
		<div class="col">
			<div class="inner">
			<?php if (isset($ttl) && trim($ttl) != "") { ?>
			<h2 class="ttl ttl--underline"><?php echo nl2br($ttl); ?></h2><?php } ?>
				<div class="txt">
					<?php if (isset($content) && trim($content) != "") { ?><?php echo $content; ?><?php } ?>
				</div>
			</div>
		</div>
	</div>
<?php } ?>