<?php defined("C5_EXECUTE") or die("Access Denied."); ?>
<div class="table_wrap__row">
	<h4 class="th"><?php if (isset($ttl) && trim($ttl) != "") { ?><?php echo nl2br($ttl); ?><?php } ?></h4>
	<div class="td">
		<?php if (isset($content) && trim($content) != "") { ?>
		<?php echo $content; ?><?php } ?>
	</div>
</div>
