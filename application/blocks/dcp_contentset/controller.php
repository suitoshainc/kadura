<?php namespace Application\Block\DcpContentset;

defined("C5_EXECUTE") or die("Access Denied.");

use Concrete\Core\Block\BlockController;
use Core;
use Database;
use File;
use Page;
use Concrete\Core\Editor\LinkAbstractor;
use AssetList;

class Controller extends BlockController
{
    public $helpers = array('form');
    public $btFieldsRequired = array('images' => array());
    protected $btExportFileColumns = array('img');
    protected $btExportPageColumns = array();
    protected $btExportTables = array('btDcpContentset', 'btDcpContentsetImagesEntries');
    protected $btTable = 'btDcpContentset';
    protected $btInterfaceWidth = 400;
    protected $btInterfaceHeight = 500;
    protected $btIgnorePageThemeGridFrameworkContainer = false;
    protected $btCacheBlockRecord = true;
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btCacheBlockOutputForRegisteredUsers = true;
    protected $btCacheBlockOutputLifetime = 0;
    protected $pkg = false;
    
    public function getBlockTypeDescription()
    {
        return t("");
    }

    public function getBlockTypeName()
    {
        return t("タイトル+テキスト+画像");
    }

    public function getSearchableContent()
    {
        $content = array();
        $content[] = $this->ttl;
        $content[] = $this->content;
        return implode(" ", $content);
    }

    public function view()
    {
        $db = Database::connection();
        $images = array();
        $images_items = $db->fetchAll('SELECT * FROM btDcpContentsetImagesEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($images_items as $images_item_k => &$images_item_v) {
            if (isset($images_item_v['img']) && trim($images_item_v['img']) != "" && ($f = File::getByID($images_item_v['img'])) && is_object($f)) {
                $images_item_v['img'] = $f;
            } else {
                $images_item_v['img'] = false;
            }
        }
        $this->set('images_items', $images_items);
        $this->set('images', $images);
        $this->set('content', LinkAbstractor::translateFrom($this->content));
    }

    public function delete()
    {
        $db = Database::connection();
        $db->delete('btDcpContentsetImagesEntries', array('bID' => $this->bID));
        parent::delete();
    }

    public function duplicate($newBID)
    {
        $db = Database::connection();
        $images_items = $db->fetchAll('SELECT * FROM btDcpContentsetImagesEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($images_items as $images_item) {
            unset($images_item['id']);
            $images_item['bID'] = $newBID;
            $db->insert('btDcpContentsetImagesEntries', $images_item);
        }
        parent::duplicate($newBID);
    }

    public function add()
    {
        $this->addEdit();
        $images = $this->get('images');
        $this->set('images_items', array());
        $this->set('images', $images);
    }

    public function edit()
    {
        $db = Database::connection();
        $this->addEdit();
        $images = $this->get('images');
        $images_items = $db->fetchAll('SELECT * FROM btDcpContentsetImagesEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($images_items as &$images_item) {
            if (!File::getByID($images_item['img'])) {
                unset($images_item['img']);
            }
        }
        $this->set('images', $images);
        $this->set('images_items', $images_items);
        
        $this->set('content', LinkAbstractor::translateFromEditMode($this->content));
    }

    protected function addEdit()
    {
        $images = array();
        $this->set('images', $images);
        $this->set('identifier', new \Concrete\Core\Utility\Service\Identifier());
        $al = AssetList::getInstance();
        $al->register('css', 'repeatable-ft.form', 'blocks/dcp_contentset/css_form/repeatable-ft.form.css', array(), $this->pkg);
        $al->register('javascript', 'handlebars', 'blocks/dcp_contentset/js_form/handlebars-v4.0.4.js', array(), $this->pkg);
        $al->register('javascript', 'handlebars-helpers', 'blocks/dcp_contentset/js_form/handlebars-helpers.js', array(), $this->pkg);
        $this->requireAsset('core/sitemap');
        $this->requireAsset('css', 'repeatable-ft.form');
        $this->requireAsset('javascript', 'handlebars');
        $this->requireAsset('javascript', 'handlebars-helpers');
        $this->requireAsset('core/file-manager');
        $this->requireAsset('redactor');
        $this->set('btFieldsRequired', $this->btFieldsRequired);
        $this->set('identifier_getString', Core::make('helper/validation/identifier')->getString(18));
    }

    public function save($args)
    {
        $db = Database::connection();
        $rows = $db->fetchAll('SELECT * FROM btDcpContentsetImagesEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        $images_items = isset($args['images']) && is_array($args['images']) ? $args['images'] : array();
        $queries = array();
        if (!empty($images_items)) {
            $i = 0;
            foreach ($images_items as $images_item) {
                $data = array(
                    'sortOrder' => $i + 1,
                );
                if (isset($images_item['img']) && trim($images_item['img']) != '') {
                    $data['img'] = trim($images_item['img']);
                } else {
                    $data['img'] = null;
                }
                if (isset($rows[$i])) {
                    $queries['update'][$rows[$i]['id']] = $data;
                    unset($rows[$i]);
                } else {
                    $data['bID'] = $this->bID;
                    $queries['insert'][] = $data;
                }
                $i++;
            }
        }
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $queries['delete'][] = $row['id'];
            }
        }
        if (!empty($queries)) {
            foreach ($queries as $type => $values) {
                if (!empty($values)) {
                    switch ($type) {
                        case 'update':
                            foreach ($values as $id => $data) {
                                $db->update('btDcpContentsetImagesEntries', $data, array('id' => $id));
                            }
                            break;
                        case 'insert':
                            foreach ($values as $data) {
                                $db->insert('btDcpContentsetImagesEntries', $data);
                            }
                            break;
                        case 'delete':
                            foreach ($values as $value) {
                                $db->delete('btDcpContentsetImagesEntries', array('id' => $value));
                            }
                            break;
                    }
                }
            }
        }
        $args['content'] = LinkAbstractor::translateTo($args['content']);
        parent::save($args);
    }

    public function validate($args)
    {
        $e = Core::make("helper/validation/error");
        if (in_array("ttl", $this->btFieldsRequired) && trim($args["ttl"]) == "") {
            $e->add(t("The %s field is required.", t("タイトル")));
        }
        $imagesEntriesMin = 0;
        $imagesEntriesMax = 0;
        $imagesEntriesErrors = 0;
        $images = array();
        if (isset($args['images']) && is_array($args['images']) && !empty($args['images'])) {
            if ($imagesEntriesMin >= 1 && count($args['images']) < $imagesEntriesMin) {
                $e->add(t("The %s field requires at least %s entries, %s entered.", t("画像データ"), $imagesEntriesMin, count($args['images'])));
                $imagesEntriesErrors++;
            }
            if ($imagesEntriesMax >= 1 && count($args['images']) > $imagesEntriesMax) {
                $e->add(t("The %s field is set to a maximum of %s entries, %s entered.", t("画像データ"), $imagesEntriesMax, count($args['images'])));
                $imagesEntriesErrors++;
            }
            if ($imagesEntriesErrors == 0) {
                foreach ($args['images'] as $images_k => $images_v) {
                    if (is_array($images_v)) {
                        if (in_array("img", $this->btFieldsRequired['images']) && (!isset($images_v['img']) || trim($images_v['img']) == "" || !is_object(File::getByID($images_v['img'])))) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("画像"), t("画像データ"), $images_k));
                        }
                    } else {
                        $e->add(t("The values for the %s field, row #%s, are incomplete.", t('画像データ'), $images_k));
                    }
                }
            }
        } else {
            if ($imagesEntriesMin >= 1) {
                $e->add(t("The %s field requires at least %s entries, none entered.", t("画像データ"), $imagesEntriesMin));
            }
        }
        if (in_array("content", $this->btFieldsRequired) && (trim($args["content"]) == "")) {
            $e->add(t("The %s field is required.", t("本文")));
        }
        return $e;
    }

    public function composer()
    {
        $al = AssetList::getInstance();
        $al->register('javascript', 'auto-js-dcp_contentset', 'blocks/dcp_contentset/auto.js', array(), $this->pkg);
        $this->requireAsset('javascript', 'auto-js-dcp_contentset');
        $this->edit();
    }
}