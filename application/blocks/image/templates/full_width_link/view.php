<?php defined('C5_EXECUTE') or die("Access Denied.");

$c = Page::getCurrentPage();
if (is_object($f)) {
?>
<div class="link--fullwidth link" style="background-image:url(<?php echo $f->getURL();?>);">
	<div class="inner">
		<a class="btn--basic" href="<?php echo $linkURL;?>"><?php echo h($title);?></a>
	</div>
</div>

<?php }else if ($c->isEditMode()) { ?>

<div class="ccm-edit-mode-disabled-item"><?php echo t('Empty Image Block.')?></div>

<?php } ?>

