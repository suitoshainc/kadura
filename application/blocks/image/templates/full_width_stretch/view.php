<?php defined('C5_EXECUTE') or die("Access Denied.");

$c = Page::getCurrentPage();
if (is_object($f)) {
?>
<div class="img--fullwidth" style="background-image:url(<?php echo $f->getURL();?>);">
	<img src="<?php echo $f->getURL();?>" alt="">
</div>

<?php }else if ($c->isEditMode()) { ?>

<div class="ccm-edit-mode-disabled-item"><?php echo t('Empty Image Block.')?></div>

<?php } ?>

