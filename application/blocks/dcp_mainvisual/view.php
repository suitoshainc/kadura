<?php defined("C5_EXECUTE") or die("Access Denied."); ?>

<?php if (!empty($images_items)) { ?>
<div class="slide">
<?php if ($cach) { ?>
<img class="slide_text" src="<?php echo $cach->getURL(); ?>" alt="<?php echo $cach->getTitle(); ?>"/><?php } ?>
<ul>
	<?php foreach ($images_items as $images_item_key => $images_item) { ?>
	<li>
		<?php if ($images_item["imgL"]) { ?>
			<div class="half half1" style="background-image:url(<?php echo $images_item["imgL"]->getURL(); ?>);">
				<img src="<?php echo $images_item["imgL"]->getURL(); ?>" alt="<?php echo $images_item["imgL"]->getTitle(); ?>"/>
			</div>
		<?php } ?>
		<?php if ($images_item["imgR"]) { ?>
			<div class="half half2" style="background-image:url(<?php echo $images_item["imgR"]->getURL(); ?>);">
				<img src="<?php echo $images_item["imgR"]->getURL(); ?>" alt="<?php echo $images_item["imgR"]->getTitle(); ?>"/>
			</div>
		<?php } ?>
	</li>
	<?php } ?>
</ul>
</div>
<?php } ?>