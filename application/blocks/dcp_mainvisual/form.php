<?php defined("C5_EXECUTE") or die("Access Denied."); ?>

<?php $tabs = array(
    array('form-basics-' . $identifier_getString, t('Basics'), true),
    array('form-images_items-' . $identifier_getString, t('画像データ'))
);
echo Core::make('helper/concrete/ui')->tabs($tabs); ?>

<div class="ccm-tab-content" id="ccm-tab-content-form-basics-<?php echo $identifier_getString; ?>">
    <div class="form-group">
    <?php
    if (isset($cach) && $cach > 0) {
        $cach_o = File::getByID($cach);
        if (!is_object($cach_o)) {
            unset($cach_o);
        }
    } ?>
    <?php echo $form->label('cach', t("キャッチコピー")); ?>
    <?php echo isset($btFieldsRequired) && in_array('cach', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php echo Core::make("helper/concrete/asset_library")->image('ccm-b-dcp_mainvisual-cach-' . $identifier_getString, $view->field('cach'), t("Choose Image"), $cach_o); ?>
</div>
</div>

<div class="ccm-tab-content" id="ccm-tab-content-form-images_items-<?php echo $identifier_getString; ?>">
    <script type="text/javascript">
    var CCM_EDITOR_SECURITY_TOKEN = "<?php echo Core::make('helper/validation/token')->generate('editor')?>";
</script>
<?php $repeatable_container_id = 'btDcpMainvisual-images-container-' . $identifier_getString; ?>
    <div id="<?php echo $repeatable_container_id; ?>">
        <div class="sortable-items-wrapper">
            <a href="#" class="btn btn-primary add-entry">
                <?php echo t('Add Entry'); ?>
            </a>

            <div class="sortable-items" data-attr-content="<?php echo htmlspecialchars(
                json_encode(
                    array(
                        'items' => $images_items,
                        'order' => array_keys($images_items),
                    )
                )
            ); ?>">
            </div>

            <a href="#" class="btn btn-primary add-entry add-entry-last">
                <?php echo t('Add Entry'); ?>
            </a>
        </div>

        <script class="repeatableTemplate" type="text/x-handlebars-template">
            <div class="sortable-item" data-id="{{id}}">
                <div class="sortable-item-title">
                    <span class="sortable-item-title-default">
                        <?php echo t('画像データ') . ' ' . t("row") . ' <span>#{{id}}</span>'; ?>
                    </span>
                    <span class="sortable-item-title-generated"></span>
                </div>

                <div class="sortable-item-inner">            <div class="form-group">
    <label for="<?php echo $view->field('images'); ?>[{{id}}][imgL]" class="control-label"><?php echo t("左画像"); ?></label>
    <?php echo isset($btFieldsRequired['images']) && in_array('imgL', $btFieldsRequired['images']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <div data-file-selector-input-name="<?php echo $view->field('images'); ?>[{{id}}][imgL]" class="ccm-file-selector ft-image-imgL-file-selector" data-file-selector-f-id="{{ imgL }}"></div>
</div>            <div class="form-group">
    <label for="<?php echo $view->field('images'); ?>[{{id}}][imgR]" class="control-label"><?php echo t("右画像"); ?></label>
    <?php echo isset($btFieldsRequired['images']) && in_array('imgR', $btFieldsRequired['images']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <div data-file-selector-input-name="<?php echo $view->field('images'); ?>[{{id}}][imgR]" class="ccm-file-selector ft-image-imgR-file-selector" data-file-selector-f-id="{{ imgR }}"></div>
</div></div>

                <span class="sortable-item-collapse-toggle"></span>

                <a href="#" class="sortable-item-delete" data-attr-confirm-text="<?php echo t('Are you sure'); ?>">
                    <i class="fa fa-times"></i>
                </a>

                <div class="sortable-item-handle">
                    <i class="fa fa-sort"></i>
                </div>
            </div>
        </script>
    </div>

<script type="text/javascript">
    Concrete.event.publish('btDcpMainvisual.images.edit.open', {id: '<?php echo $repeatable_container_id; ?>'});
    $.each($('#<?php echo $repeatable_container_id; ?> input[type="text"].title-me'), function () {
        $(this).trigger('keyup');
    });
</script>
</div>