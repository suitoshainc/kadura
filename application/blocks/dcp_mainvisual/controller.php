<?php namespace Application\Block\DcpMainvisual;

defined("C5_EXECUTE") or die("Access Denied.");

use Concrete\Core\Block\BlockController;
use Core;
use File;
use Page;
use Database;
use AssetList;

class Controller extends BlockController
{
    public $helpers = array('form');
    public $btFieldsRequired = array('images' => array('imgL', 'imgR'));
    protected $btExportFileColumns = array('cach', 'imgL', 'imgR');
    protected $btExportPageColumns = array();
    protected $btExportTables = array('btDcpMainvisual', 'btDcpMainvisualImagesEntries');
    protected $btTable = 'btDcpMainvisual';
    protected $btInterfaceWidth = 400;
    protected $btInterfaceHeight = 500;
    protected $btIgnorePageThemeGridFrameworkContainer = false;
    protected $btCacheBlockRecord = true;
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btCacheBlockOutputForRegisteredUsers = true;
    protected $btCacheBlockOutputLifetime = 0;
    protected $pkg = false;
    
    public function getBlockTypeDescription()
    {
        return t("");
    }

    public function getBlockTypeName()
    {
        return t("メインビジュアル");
    }

    public function view()
    {
        $db = Database::connection();
        
        if ($this->cach && ($f = File::getByID($this->cach)) && is_object($f)) {
            $this->set("cach", $f);
        } else {
            $this->set("cach", false);
        }
        $images = array();
        $images_items = $db->fetchAll('SELECT * FROM btDcpMainvisualImagesEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($images_items as $images_item_k => &$images_item_v) {
            if (isset($images_item_v['imgL']) && trim($images_item_v['imgL']) != "" && ($f = File::getByID($images_item_v['imgL'])) && is_object($f)) {
                $images_item_v['imgL'] = $f;
            } else {
                $images_item_v['imgL'] = false;
            }
            if (isset($images_item_v['imgR']) && trim($images_item_v['imgR']) != "" && ($f = File::getByID($images_item_v['imgR'])) && is_object($f)) {
                $images_item_v['imgR'] = $f;
            } else {
                $images_item_v['imgR'] = false;
            }
        }
        $this->set('images_items', $images_items);
        $this->set('images', $images);
    }

    public function delete()
    {
        $db = Database::connection();
        $db->delete('btDcpMainvisualImagesEntries', array('bID' => $this->bID));
        parent::delete();
    }

    public function duplicate($newBID)
    {
        $db = Database::connection();
        $images_items = $db->fetchAll('SELECT * FROM btDcpMainvisualImagesEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($images_items as $images_item) {
            unset($images_item['id']);
            $images_item['bID'] = $newBID;
            $db->insert('btDcpMainvisualImagesEntries', $images_item);
        }
        parent::duplicate($newBID);
    }

    public function add()
    {
        $this->addEdit();
        $images = $this->get('images');
        $this->set('images_items', array());
        $this->set('images', $images);
    }

    public function edit()
    {
        $db = Database::connection();
        $this->addEdit();
        $images = $this->get('images');
        $images_items = $db->fetchAll('SELECT * FROM btDcpMainvisualImagesEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($images_items as &$images_item) {
            if (!File::getByID($images_item['imgL'])) {
                unset($images_item['imgL']);
            }
        }
        foreach ($images_items as &$images_item) {
            if (!File::getByID($images_item['imgR'])) {
                unset($images_item['imgR']);
            }
        }
        $this->set('images', $images);
        $this->set('images_items', $images_items);
    }

    protected function addEdit()
    {
        $images = array();
        $this->set('images', $images);
        $this->set('identifier', new \Concrete\Core\Utility\Service\Identifier());
        $al = AssetList::getInstance();
        $al->register('css', 'repeatable-ft.form', 'blocks/dcp_mainvisual/css_form/repeatable-ft.form.css', array(), $this->pkg);
        $al->register('javascript', 'handlebars', 'blocks/dcp_mainvisual/js_form/handlebars-v4.0.4.js', array(), $this->pkg);
        $al->register('javascript', 'handlebars-helpers', 'blocks/dcp_mainvisual/js_form/handlebars-helpers.js', array(), $this->pkg);
        $this->requireAsset('core/file-manager');
        $this->requireAsset('core/sitemap');
        $this->requireAsset('css', 'repeatable-ft.form');
        $this->requireAsset('javascript', 'handlebars');
        $this->requireAsset('javascript', 'handlebars-helpers');
        $this->set('btFieldsRequired', $this->btFieldsRequired);
        $this->set('identifier_getString', Core::make('helper/validation/identifier')->getString(18));
    }

    public function save($args)
    {
        $db = Database::connection();
        $rows = $db->fetchAll('SELECT * FROM btDcpMainvisualImagesEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        $images_items = isset($args['images']) && is_array($args['images']) ? $args['images'] : array();
        $queries = array();
        if (!empty($images_items)) {
            $i = 0;
            foreach ($images_items as $images_item) {
                $data = array(
                    'sortOrder' => $i + 1,
                );
                if (isset($images_item['imgL']) && trim($images_item['imgL']) != '') {
                    $data['imgL'] = trim($images_item['imgL']);
                } else {
                    $data['imgL'] = null;
                }
                if (isset($images_item['imgR']) && trim($images_item['imgR']) != '') {
                    $data['imgR'] = trim($images_item['imgR']);
                } else {
                    $data['imgR'] = null;
                }
                if (isset($rows[$i])) {
                    $queries['update'][$rows[$i]['id']] = $data;
                    unset($rows[$i]);
                } else {
                    $data['bID'] = $this->bID;
                    $queries['insert'][] = $data;
                }
                $i++;
            }
        }
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $queries['delete'][] = $row['id'];
            }
        }
        if (!empty($queries)) {
            foreach ($queries as $type => $values) {
                if (!empty($values)) {
                    switch ($type) {
                        case 'update':
                            foreach ($values as $id => $data) {
                                $db->update('btDcpMainvisualImagesEntries', $data, array('id' => $id));
                            }
                            break;
                        case 'insert':
                            foreach ($values as $data) {
                                $db->insert('btDcpMainvisualImagesEntries', $data);
                            }
                            break;
                        case 'delete':
                            foreach ($values as $value) {
                                $db->delete('btDcpMainvisualImagesEntries', array('id' => $value));
                            }
                            break;
                    }
                }
            }
        }
        parent::save($args);
    }

    public function validate($args)
    {
        $e = Core::make("helper/validation/error");
        if (in_array("cach", $this->btFieldsRequired) && (trim($args["cach"]) == "" || !is_object(File::getByID($args["cach"])))) {
            $e->add(t("The %s field is required.", t("キャッチコピー")));
        }
        $imagesEntriesMin = 0;
        $imagesEntriesMax = 0;
        $imagesEntriesErrors = 0;
        $images = array();
        if (isset($args['images']) && is_array($args['images']) && !empty($args['images'])) {
            if ($imagesEntriesMin >= 1 && count($args['images']) < $imagesEntriesMin) {
                $e->add(t("The %s field requires at least %s entries, %s entered.", t("画像データ"), $imagesEntriesMin, count($args['images'])));
                $imagesEntriesErrors++;
            }
            if ($imagesEntriesMax >= 1 && count($args['images']) > $imagesEntriesMax) {
                $e->add(t("The %s field is set to a maximum of %s entries, %s entered.", t("画像データ"), $imagesEntriesMax, count($args['images'])));
                $imagesEntriesErrors++;
            }
            if ($imagesEntriesErrors == 0) {
                foreach ($args['images'] as $images_k => $images_v) {
                    if (is_array($images_v)) {
                        if (in_array("imgL", $this->btFieldsRequired['images']) && (!isset($images_v['imgL']) || trim($images_v['imgL']) == "" || !is_object(File::getByID($images_v['imgL'])))) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("左画像"), t("画像データ"), $images_k));
                        }
                        if (in_array("imgR", $this->btFieldsRequired['images']) && (!isset($images_v['imgR']) || trim($images_v['imgR']) == "" || !is_object(File::getByID($images_v['imgR'])))) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("右画像"), t("画像データ"), $images_k));
                        }
                    } else {
                        $e->add(t("The values for the %s field, row #%s, are incomplete.", t('画像データ'), $images_k));
                    }
                }
            }
        } else {
            if ($imagesEntriesMin >= 1) {
                $e->add(t("The %s field requires at least %s entries, none entered.", t("画像データ"), $imagesEntriesMin));
            }
        }
        return $e;
    }

    public function composer()
    {
        $al = AssetList::getInstance();
        $al->register('javascript', 'auto-js-dcp_mainvisual', 'blocks/dcp_mainvisual/auto.js', array(), $this->pkg);
        $this->requireAsset('javascript', 'auto-js-dcp_mainvisual');
        $this->edit();
    }
}