<?php
defined('C5_EXECUTE') or die("Access Denied.");
$th = Loader::helper('text');
$c = Page::getCurrentPage();
$dh = Core::make('helper/date'); /* @var $dh \Concrete\Core\Localization\Service\Date */
?>

<?php if ( $c->isEditMode() && $controller->isBlockEmpty()) { ?>
<div class="ccm-edit-mode-disabled-item"><?php echo t('Empty Page List Block.')?></div>
<?php } else { ?>

<section id="news">

	<?php if (isset($pageListTitle) && $pageListTitle): ?>
		<h2 class="ttl--underline--center"><?php echo h($pageListTitle)?></h2>
	<?php endif; ?>
<div class="layout--4col">
	<?php
	foreach ($pages as $page):

	// Prepare data for each page being listed...
	$buttonClasses = 'ccm-block-page-list-read-more';
	$entryClasses = 'ccm-block-page-list-page-entry';
	$title = $th->entities($page->getCollectionName());
	$url = ($page->getCollectionPointerExternalLink() != '') ? $page->getCollectionPointerExternalLink() : $nh->getLinkToCollection($page);
	$target = ($page->getCollectionPointerExternalLink() != '' && $page->openCollectionPointerExternalLinkInNewWindow()) ? '_blank' : $page->getAttribute('nav_target');
	$target = empty($target) ? '_self' : $target;
	$description = $page->getCollectionDescription();
	$description = $controller->truncateSummaries ? $th->wordSafeShortText($description, $controller->truncateChars) : $description;
	$description = $th->entities($description);
	$thumbnail = false;
	if ($displayThumbnail) {
		$thumbnail = $page->getAttribute('thumbnail');
	}
	if (is_object($thumbnail) && $includeEntryText) {
		$entryClasses = 'ccm-block-page-list-page-entry-horizontal';
	}

	$date = $dh->date('Y/m/d',strtotime($page->getCollectionDatePublic()));
	?>
		<div class="col <?php echo $entryClasses?>">
			<a href="<?php echo $url;?>" <?php echo $target;?>>
				<?php if (is_object($thumbnail)): ?>
					<div class="img" style="background-image:url(<?php echo $thumbnail->getUrl();?>);"></div>
				<?php endif; ?>
			<p class="date"><?php echo $date;?></p>
			<h3 class="ttl"><?php echo $title;?></h3>
			</a>
		</div>
	<?php endforeach; ?>
</div>
<a class="btn--basic--black" href="#">ニュース一覧を見る</a>
</section>
<?php } ?>
