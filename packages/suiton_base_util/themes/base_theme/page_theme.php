<?php
namespace Concrete\Package\SuitonBaseUtil\Theme\BaseTheme;
defined('C5_EXECUTE') or die("Access Denied.");

use Concrete\Core\Area\Layout\Preset\Provider\ThemeProviderInterface;
use Concrete\Core\Page\Theme\Theme;

class PageTheme extends Theme implements ThemeProviderInterface
{
	/**
	 * テーマで利用するアセットファイル群の定義
	 */
	public function registerAssets() {
		$this->requireAsset('javascript', 'jquery');
		$this->requireAsset('javascript', 'jquery/ui');
		$this->providesAsset('css', 'css/font-awesome.css');

		/* my css
		package/controllerで定義したアセットハンドルを配列に記述
		----------------------- */
		$local_css = array(
			'bxslider-css',
			'awesome-css',
			'style-css'
		);
		foreach($local_css as $css){
			$this->requireAsset('css', $css);
		}

		/* my js
		package/controllerで定義したアセットハンドルを配列に記述
		----------------------- */
		$local_js = array(
			'maps.api',
			'lodash-js',
			'bundle-js',
		);
		foreach($local_js as $js){
			$this->requireAsset('javascript', $js);
		}
	}
	/**
	 * 共通で設定したいカスタムクラス
	 *
	 *
	 * @return array
	 */
	public function getCommonClass(){
		$classes = array(
			'mt0','mt80','mt100' ,'base_width' , 'table_wrap' , 'ttl--underline--center'
		);
		return $classes;
	}
	/**
	 * エリアのカスタムクラス設定時にデフォルト登録しておくクラス
	 *
	 * @return array
	 */
	public function getThemeAreaClasses()
	{
		$classes = $this->getCommonClass();
		return array(
			'Main' => $classes
		);
	}
	/**
	 * ブロックのカスタムクラス設定時にデフォルト登録しておくクラス
	 *
	 * @return array
	 */
	public function getThemeBlockClasses()
	{
		$classes = $this->getCommonClass();
		return array(
			'core_area_layout' => array_merge($classes,array('responsive--1col')),
			'image' => $classes,
			'content' => $classes,
		);
	}
	/**
	 * エディタのカスタムスタイル設定時にデフォルト登録しておくクラス
	 *
	 * @return array
	 */
	public function getThemeEditorClasses()
	{
		return array(
			array(
				'title' => t('明朝フォント'),
				'menuClass' => '',
				'spanClass' => 'font--yu_mincho',
				'forceBlock' => -1
			),
			array(
				'title' => t('幅100%画像'),
				'menuClass' => '',
				'spanClass' => 'fullwidth',
				'forceBlock' => -1
			),
			array(
				'title' => t('グレー・サイズ小'),
				'menuClass' => '',
				'spanClass' => 'small_text',
				'forceBlock' => 1
			),
			array(
				'title' => t('サブタイトル'),
				'menuClass' => '',
				'spanClass' => 'ttl_sub',
				'forceBlock' => -1
			),
			array(
				'title' => t('ボタン：黒枠'),
				'menuClass' => '',
				'spanClass' => 'btn--basic--black inline-block',
				'forceBlock' => -1
			),
		);
	}
	/**
	 * カスタムレイアウトプリセット
	 * https://concrete5-japan.org/help/5-7/developer/designing-for-concrete5/adding-complex-custom-layout-presets-in-your-theme/
	 * @return array
	 */
	public function getThemeAreaLayoutPresets()
	{
		$presets = [
			[
				'handle' => 'section_wrapper',
				'name' => 'section要素で囲む',
				'container' => '<section></section>',
				'columns' => [
					'<div class="section_wrapper_inner"></div>',
				],
			],
			[
				'handle' => 'layout--2col',
				'name' => 'フルサイズ2カラム',
				'container' => '<section class="layout--2col by_ccm_layout"></section>',
				'columns' => [
					'<div class="col"></div>',
					'<div class="col"></div>'
				],
			]
		];

		return $presets;
	}
}
