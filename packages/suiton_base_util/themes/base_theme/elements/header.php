<?php
defined('C5_EXECUTE') or die(_("Access Denied."));
$u = new User();
?>
<!DOCTYPE html><!-- [if IEMobile 7 ]>  <html lang="ja-JP" class="no-js iem7"><![endif] -->
<!-- [if lt IE 7]><html lang="ja-JP" class="no-js lt-ie9 lt-ie8 lt-ie7 ie6"><![endif] -->
<!-- [if IE 7]><html lang="ja-JP" class="no-js lt-ie9 lt-ie8 ie7"><![endif] -->
<!-- [if IE 8]><html lang="ja-JP" class="no-js lt-ie9 ie8"><![endif] -->

<!-- [if (gte IE 8)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!--><html lang="ja-JP" class="no-js"><!--<![endif]-->
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta charset="UTF-8">
	<meta id="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=5.0,user-scalable=1" name="viewport">
	<?php Loader::element('header_required');?>
	<script>var CCM_THEME_PATH = '<?php echo $this->getThemePath()?>'</script>
	<?php
	//for total pagenation num
	//for ajax pagenation,for infinityscroll etc...
	?>
	<script>var cm_this_pagenation_total = 0;</script>
<?php
if($c->isEditMode()){
	//編集モード時に適用したい処理
}

if($u->isLoggedIn()) {
	//ログイン状態で適用したい処理
}

$nh = Core::make('helper/navigation');
$trail = $nh->getTrailToCollection($c);
$ancestors = array_reverse($trail);
?>
</head>

<body id="top">
<div class="<?php echo $c->getPageWrapperClass();?><?php if(count($ancestors) > 0) echo ' lower';?>">
	<div id="wrap">
		<nav class="spnavi sp" id="spnavi">
			<div id="scroller">
				<ul>
					<li><a href="">Concept</a></li>
					<li class="has_child"><a href="">Products</a>
						<ul class="child">
							<li><a href="">Antiques</a></li>
							<li><a href="">Furniture</a></li>
							<li><a href="">Jewelry</a></li>
						</ul>
					</li>
					<li><a href="">News</a></li>
					<li><a href="">Access</a></li>
				</ul>
			</div>
		</nav>
		<div class="btn_sp_navi_wrap"><a class="btn_sp_navi nosc nosms" href="#sp"><span class="bar"></span><span class="bar"></span><span class="bar"></span><span class="text">Menu</span><span class="text--active">Close</span></a></div>
		<div class="header_wrapper">
			<header class="header" id="header">
				<h1 class="logo"><a href="<?php echo BASE_URL;?>"><span>かづらせい</span></a></h1>
			</header>
			<nav class="gnav">
				<ul>
					<li><a href="">Concept</a></li>
					<li class="has_child"><a href="">Products</a>
						<ul class="child">
							<li><a href="">Antiques</a></li>
							<li><a href="">Furniture</a></li>
							<li><a href="">Jewelry</a></li>
						</ul>
					</li>
					<li><a href="">News</a></li>
					<li><a href="">Access</a></li>
				</ul>
			</nav>
		</div>
