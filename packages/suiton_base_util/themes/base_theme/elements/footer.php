<?php defined('C5_EXECUTE') or die("Access Denied.");$u = new User();?>

<?php
if($c->getAttribute('hide_shop_info') !== '1'):?>
<section id="shop">
	<h2 class="ttl--underline--center">Shop</h2>
	<div id="map"></div>
	<div class="footer_info">
		<div class="inner">
			<div class="table_wrap">
			<?php
				$a = new GlobalArea('Shop Info 1');
				$a->display();
			?>
			</div>
		</div>
		<div class="inner">
			<div class="table_wrap">
			<?php
				$a = new GlobalArea('Shop Info 2');
				$a->display();
			?>
			</div>
		</div>
	</div>
</section>
<?php endif;?>
<div id="footer">
	<div id="totop"><a href="#top"><i class="fa fa-angle-up" aria-hidden="true"></i></a></div>
	<nav class="spnavi sp">
		<ul>
			<li><a href="">Concept</a></li>
			<li class="has_child"><a href="">Products</a>
				<ul class="child">
					<li><a href="">Antiques</a></li>
					<li><a href="">Furniture</a></li>
					<li><a href="">Jewelry</a></li>
				</ul>
			</li>
			<li><a href="">News</a></li>
			<li><a href="">Access</a></li>
		</ul>
	</nav>
	<div class="inner">
		<p>
		<?php
			if(!$u->isLoggedIn()) {
				echo '<small>';
			}
			$a = new GlobalArea('Copyright');
			$a->display();
			if(!$u->isLoggedIn()) {
				echo '</small>';
			}
		?>
		</p>
	</div>
</div>
<div id="responsive_flg"></div>
</div><!--/#wrap-->
</div><!--/getPageWrapperClass-->
<?php Loader::element('footer_required'); ?>
</body></html>
